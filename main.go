package main

import (
	"bufio"
	"fmt"
	"github.com/tarm/goserial"
	"golang.org/x/net/websocket"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

var serialChan chan string
var serialRW io.ReadWriteCloser
var wsConn *websocket.Conn

func logErr(e error) {
	if e != nil {
		fmt.Println("ERROR: " + e.Error())
		websocket.Message.Send(wsConn, "#"+e.Error())
	}
}

func serialWriter() {
	for {
		if serialRW == nil {
			fmt.Println("Serial port not open!")
			return
		}

		msg := <-serialChan
		if _, err := serialRW.Write([]byte(msg)); err != nil {
			logErr(err)
		} else {
			fmt.Printf("Msg: '%v' successfuly sending\n", msg)
		}
	}
}

func serialReader() {
	reader := bufio.NewReader(serialRW)

	for {
		if serialRW == nil {
			fmt.Println("Serial port not open!")
			return
		}

		msg, err := reader.ReadBytes('\x0a')
		logErr(err)

		fmt.Printf("Serial msg receive: %v", string(msg))
		websocket.Message.Send(wsConn, "serial msg "+string(msg))
	}
}

func configPort(msg string) {
	splArr := strings.Split(msg, " ")
	serialPath := splArr[1]
	spd, e := strconv.Atoi(splArr[2])
	logErr(e)

	fmt.Printf("Open com port: serial %v, speed %v\n", serialPath, spd)

	c := &serial.Config{Name: serialPath, Baud: spd}

	var err error

	serialRW, err = serial.OpenPort(c)
	if err != nil {
		logErr(err)
	} else {
		serialChan = make(chan string)
		go serialWriter()
		go serialReader()
	}
}

func handle(msg []byte) {
	smsg := string(msg)

	if strings.HasPrefix(smsg, "!open") {
		configPort(smsg)
	}

	if strings.HasPrefix(smsg, "!send") {
		str := strings.SplitN(smsg, " ", 2)

		if len(str) > 1 {
			serialChan <- strings.SplitN(smsg, " ", 2)[1]
		}
	}
}

func Echo(ws *websocket.Conn) {
	wsConn = ws
	ws.Write([]byte("Connect successfuly"))
	fmt.Println("Connect successfuly")

	for {
		var msg []byte
		e := websocket.Message.Receive(ws, &msg)
		if e != nil {
			logErr(e)
			fmt.Println("Socket close")
			return
		}

		fmt.Println("<= '" + string(msg) + "'")
		go handle(msg)
		websocket.Message.Send(ws, &msg)
	}
}

func IndexPage(w http.ResponseWriter, r *http.Request) {
	tmp, err := ioutil.ReadFile("./index.html")
	logErr(err)
	fmt.Fprintf(w, string(tmp))
}

func main() {
	http.Handle("/echo", websocket.Handler(Echo))
	http.HandleFunc("/", IndexPage)
	err := http.ListenAndServe(":8080", nil)

	logErr(err)
}
